from sanic import Sanic
from sanic.blueprints import Blueprint

app = Sanic(__name__)
app.static('/', './index.html')
app.static('/nodes.json', './nodes.json')
app.run(host="0.0.0.0", port=3000)