import asyncio
import time
from aio_tcpserver import tcp_server, listener
import json as js 

from aiohttp import web

SOURCE = "source_id"
TARGET = "target_id"

json = {
    "nodes" : [],
    "links" : []
}

routes = {}
nodes = {}
addresses = {}
counts = {}

node_id = 0

def dump():    
    with open("nodes.json", "w") as f:
        js.dump(json, f)

def add_link(a, b):    
    global node_id, nodes, addresses
    a_id, b_id = nodes[a], nodes[b]
    if b_id < a_id: 
        a_id, b_id = b_id, a_id    
    try:
        json["links"].index({SOURCE: a_id, TARGET: b_id})
    except:
        json["links"].append({SOURCE: a_id, TARGET: b_id})        
        dump()    

def add_route(a, route):
    global routes 
    n_id = nodes[a]
    route = "API" if ".API" in route else route
    routes[a] = route    
    json["nodes"] = [n if n["id"] != n_id else {"id": n_id, "name": route, "count": n["count"]} for n in json["nodes"]]
    dump()

def remove_link(a, b):
    global json


def add_node(origin):
    global nodes, node_id, json
    if not origin in nodes:
        node_id = node_id + 1 
        nodes[origin] = node_id                        
        json["nodes"].append({"id": node_id, "name": origin, "count": 0})        
        dump()

def node_address(peername, origin):
    global addresses
    addresses[peername] = origin

def add_count(origin, count):
    global counts 
    n_id = nodes[origin]    
    json["nodes"] = [n if n["id"] != n_id else {"id": n_id, "name": n["name"], "count": count} for n in json["nodes"]]
    dump()

def remove_node(peername):    
    global json
    if peername in addresses:
        origin = addresses[peername]
        n_id = nodes[origin]

        json["nodes"] = [n for n in json["nodes"] if n["id"] != n_id]
        json["links"] = [l for l in json["links"] if l[TARGET] != n_id and l[SOURCE] != n_id]
        del nodes[origin]
        del addresses[peername]
        dump()



class EchoServerProtocol(asyncio.StreamReaderProtocol):
    def __init__(self):
        self.loop = asyncio.get_event_loop()
        self._connection_lost = None
        self._over_ssl = False
        self._paused = None
        self._stream_writer = None
        self._stream_reader = asyncio.StreamReader(loop=self.loop)        
        self.peername = ""

    def connection_made(self, transport):
        self.peername = transport.get_extra_info('peername')
        print('Connection from {}'.format(self.peername))
        self.transport = transport
        self._connection_lost = False
        self._paused = False
        self._stream_writer = asyncio.StreamWriter(
            transport, self, self._stream_reader, self.loop)
        asyncio.ensure_future(self.get_request())


    async def get_request(self):
        try:
            while True:
                data = await self._stream_reader.readuntil(b"\n") 
                data = data.decode().strip()
                print(data)
                command, peers = data.split(" ")                
                
                if command == "ME":
                    me, route = peers.split('-')
                    add_node(me)    
                    add_route(me, route)
                    node_address(self.peername, me)
                if command == "ADD":                    
                    a, b = peers.split('-')
                    add_node(a)
                    add_node(b)
                    node_address(self.peername, a)
                    add_link(a, b)                
                if command == "COUNT":
                    me, count = peers.split("-")
                    add_node(me)
                    add_count(me, count)                    
                
        except asyncio.streams.IncompleteReadError :
            print("disconnect", self.peername)
            remove_node(self.peername)


def main():
    dump()
    tcp_server(EchoServerProtocol, worker=1)

if __name__ == '__main__':
    main()