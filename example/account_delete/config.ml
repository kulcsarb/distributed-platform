open Mirage

let stack = generic_stackv4 default_network
let http_srv = http_server @@ conduit_direct ~tls:false stack


let listeners =
  	let doc = Key.Arg.info ~doc:"TCP port for accepting transaction results." ["listen"] in
  	Key.(create "listeners" Arg.(opt (list int) [] doc))

let listen_prometheus =
  	let doc = Key.Arg.info ~doc:"TCP port for providing metrics to Prometheus." ["listen-prometheus"] in
  	Key.(create "prometheus_server_port" Arg.(opt (some int) None doc))

let links = 
	let doc = Key.Arg.info ~doc: "ip:port of node to connect to" ["links"] in 
	Key.(create "links" Arg.(opt (list string) [] doc))

let status = 
	let doc = Key.Arg.info ~doc: "status server link" ["status"] in 
	Key.(create "status" Arg.(opt string "127.0.0.1:5000" doc))

let packages = [
				"duration"; 								
				"rhyno_platform";
				"example_app"
				]

let api = 
	let packages = List.map (fun p -> package p) packages in
	let keys = [
				(Key.abstract listeners);
				(Key.abstract links);
				(Key.abstract listen_prometheus);		
				(Key.abstract status);		
				] 
	in
	foreign 
		~keys
		~packages
		"Unikernel.Main" 
		(http @-> stackv4 @-> time @-> job)


let () = 
	register "account_delete" [api $ http_srv $ stack $ default_time]	
