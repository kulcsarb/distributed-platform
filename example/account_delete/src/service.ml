open Lwt.Infix
open Example_app

let log_src = Logs.Src.create ("ACCOUNT.DELETE") ~doc:"API"
module Log = (val Logs.src_log log_src: Logs.LOG)

module Make (Network: Wrapper.S) = struct
	
	let response id : T.result = 
		Success

	let account_deleted_event id : T.event = 
		AccountDeleted {id}

	let on_task_account_delete (header: T.header) (task: T.task) =		
		match task with 
			| DeleteAccountReq req ->
				Log.info (fun f -> f "--> DeleteAccountReq");
				response req.id
				|> Network.result_for header
				
				>>= fun _ ->
				account_deleted_event req.id
				|> Network.event "account.deleted"

				>>= fun _ -> Lwt.return_unit
				
			| _ ->
				Lwt.return_unit

end

