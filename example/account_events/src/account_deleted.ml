open Example_app

let log_src = Logs.Src.create ("ACCOUNT EVENTS") ~doc:""
module Log = (val Logs.src_log log_src: Logs.LOG)

module Make (Network: Wrapper.S) = struct

	let handle (_header: T.header) (event: T.event) =
		match event with	
	        | AccountDeleted e ->  
            	Log.info (fun f -> f "--> AccountDeleted: %d" e.id);
            	Lwt.return_unit
            | _ -> 
				Lwt.return_unit
end
