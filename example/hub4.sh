trap ctrl_c INT

DEBUG_LEVEL=debug

hub/hub --listen 10004 --links 127.0.0.1:10003 --logs=$DEBUG_LEVEL &