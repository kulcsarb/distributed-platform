[@@@ocaml.warning "-33-26"]
open Bin_prot
open Bin_prot.Std


type account_created_event = { id: int; name: string; balance: float }
[@@deriving bin_io]

type account_deleted_event = {id: int }
[@@deriving bin_io]

type account_create_req = { name: string; email: string; }
[@@deriving bin_io]

type account_create_resp = { id: int; }
[@@deriving bin_io]

type account_delete_req = {	id: int; }
[@@deriving bin_io]

type account_delete_resp = { id: int; }
[@@deriving bin_io]


type event =
	| AccountCreated of account_created_event
	| AccountDeleted of account_deleted_event
[@@deriving bin_io]

type task =
    | CreateAccountReq of account_create_req
    | DeleteAccountReq of account_delete_req
[@@deriving bin_io]

type result =
	| Ack
	| Success
    | Failed of string    
    | CreateAccountResp of account_create_resp
    | DeleteAccountResp of account_delete_resp
[@@deriving bin_io]

type payload = 
  | Task of task
  | Event of event
  | Result of result
[@@deriving bin_io]

type header = Rhyno_platform.T.application_message_header

type task_handler = Rhyno_platform.T.application_message_header -> task -> unit Lwt.t

type event_handler = Rhyno_platform.T.application_message_header -> event -> unit Lwt.t

type message_handler = Rhyno_platform.T.application_message_header -> payload -> unit Lwt.t