[@@@ocaml.warning "-32-33"]
open Lwt.Infix 
open T


module type S = sig 	
	type rpc_result = (result, [`CantRoute | `Timeout | `DecodeError]) Lwt_result.t	

	val publish: ?correlation_id: string -> ?msg_type: string -> Rhyno_platform.T.route -> Rhyno_platform.T.blob -> (unit, [> `CantRoute]) Lwt_result.t
	val subscribe: ?group: string option -> string -> T.message_handler -> unit 
	val unsubscribe: ?group: string option -> string -> unit

	val on_task: ?group: string option -> string -> T.task_handler -> unit 
	val on_event: ?group: string option -> string -> T.event_handler -> unit 	

	val task: string -> task -> (unit, [`CantRoute]) Lwt_result.t
	val rpc: ?timeout:int -> string -> task -> rpc_result
	val event: string -> event -> (unit, [`CantRoute]) Lwt_result.t
	
	val result_for: Rhyno_platform.T.application_message_header -> result -> (unit, [`CantRoute]) Lwt_result.t

	val remove_task_handler: ?group: string option -> string -> unit
	val remove_event_handler: ?group: string option -> string -> unit 

	val create_account: ?timeout:int -> account_create_req -> rpc_result
	val on_create_account: (Rhyno_platform.T.application_message_header -> task -> unit Lwt.t) -> unit
end


(*
	===========================================================

	WRAPPERS FOR ENCODING AND DECODING APPLICATION SPECIFIC TYPES FOR RHYNO PLATFORM

	===========================================================
*)


let encode (p: payload) : Rhyno_platform.T.blob = 
	Rhyno_platform.Payload.encode p bin_size_payload bin_write_payload

module Decode = struct 

	let decode (blob: Rhyno_platform.T.blob) : payload = 		
		let pos_ref = ref 8 in 
		blob
		|> Rhyno_platform.Payload.to_buff
		|> bin_read_payload ~pos_ref		
	
	let wrap f = 
		try 
			Ok (f ()) 
		with e -> 
			Logs.err (fun f -> f "decode ex: %s" @@ Printexc.to_string e);
			Error `DecodeError

	let[@warning "-8"] match_task = function | Task t -> t
	let[@warning "-8"] match_event = function | Event t -> t
	let[@warning "-8"] match_result = function | Result t -> t
	
	let payload blob : (payload, [>`DecodeError]) Pervasives.result = 
		wrap @@ fun () -> blob |> decode

	let task blob : (task, [>`DecodeError]) Pervasives.result =
		wrap @@ fun () -> blob |> decode |> match_task

	let event blob : (event, [>`DecodeError]) Pervasives.result =
		wrap @@ fun () -> blob |> decode |> match_event

	let result blob : (result, [>`DecodeError]) Pervasives.result =
		wrap @@ fun () -> blob |> decode |> match_result

end


(*
	============================================================================

	WRAPPERS FOR RHYNO PLATFORM TO HIDE ENCODING / DECODING OF MESSAGE PAYLOADS

	============================================================================
*)


module Make (Rhyno: Rhyno_platform.Api.S) : S = struct

	type rpc_result = (result, [`CantRoute | `Timeout | `DecodeError]) Lwt_result.t

	let subscribe ?(group=None) routing_key f = 
		Rhyno.subscribe ~group routing_key (fun message ->
			let header = message.header in 			
			match Decode.payload message.payload with 
			| Ok payload ->
				f header payload
			| Error _ ->
				Lwt.return_unit			
		)

	let unsubscribe ?(group=None) routing_key = 
		Rhyno.unsubscribe ~group routing_key

	let publish ?(correlation_id="") ?(msg_type="") route payload =
		Rhyno.publish ~correlation_id ~msg_type route payload

	let on_task ?(group=None) routing_key f = 		
		Rhyno.on_task ~group routing_key (fun message -> 
			let header = message.header in 
			Logs.debug (fun f -> f "App received task %s" (Rhyno_platform.Uuid.to_string header.correlation_id));
			match Decode.task message.payload with 
			| Ok task ->
				f header task 
			| Error _ ->
				Lwt.return_unit
			)

	let on_event ?(group=None) routing_key f =		
 		Rhyno.on_event ~group routing_key (fun message ->
 			let header = message.header in 
 			Logs.debug (fun f -> f "App received event %s" (Rhyno_platform.Uuid.to_string header.correlation_id));
 			match Decode.event message.payload with 
 			| Ok event -> 
 				f header event
 			| Error _ -> 
 				Lwt.return_unit			
 			)

	let task routing_key t = 
		Task t |> encode |> Rhyno.task routing_key

	let rpc ?(timeout=1) routing_key t = 	
		Task t |> encode |> Rhyno.rpc ~timeout routing_key >>= function 
		| Ok message -> 
			Lwt.return @@ Decode.result message.payload
		| Error e -> 
			Lwt_result.fail e

	let event routing_key e = 
		Event e |> encode |> Rhyno.event routing_key
	
	let result_for origin r = 
		Result r |> encode |> Rhyno.result_for origin

	let remove_task_handler ?(group=None) routing_key = 
		routing_key |> Rhyno.remove_task_handler ~group

	let remove_event_handler ?(group=None) routing_key = 
		routing_key |> Rhyno.remove_event_handler ~group
(*
	===========================================================

			APPLICATION SPECIFIC WRAPPER FUNCTIONS 

	feel free to add yours 
	===========================================================
*)

	let create_account ?(timeout=1) (req: T.account_create_req) : rpc_result = 
		rpc ~timeout "account.create" (CreateAccountReq req)

	let on_create_account f = 
		on_task "account.create" f 

end