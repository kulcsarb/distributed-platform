open Lwt.Infix
open Example_app.T

module Rhyno = Rhyno_platform.Api

module Main (CohttpServer: Cohttp_lwt.S.Server) (Ipv4:  Mirage_stack_lwt.V4) (Time: Mirage_time_lwt.S) = struct 					
	
	module RHYNO = Rhyno.Make (Ipv4) (Time)
	module NETWORK = Example_app.Wrapper.Make (RHYNO)
	module HTTP = Http_server.HTTP_SERVER (CohttpServer) (Ipv4) (NETWORK)

	let handle (_header: Example_app.T.header) (event: Example_app.T.event) =		
		Lwt.return_unit

	let start conduit ip_stack time =
		NETWORK.on_event "API" handle;

		Lwt.join [
					RHYNO.init ip_stack (Key_gen.listeners ()) (Key_gen.links ()) (Key_gen.status ());
					HTTP.init conduit;
				]

end