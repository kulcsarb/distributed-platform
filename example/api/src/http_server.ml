open Lwt.Infix
open Example_app
open Example_app.T

let log_src = Logs.Src.create ("API") ~doc:""
module Log = (val Logs.src_log log_src: Logs.LOG)


module HTTP_SERVER (Server: Cohttp_lwt.S.Server) (Ipv4:  Mirage_stack_lwt.V4) (Network: Wrapper.S) = struct 
			
	let http_port () = Key_gen.http_server_port ()
	let tx_timeout () = Key_gen.tx_timeout ()

	let error_message msg = 
		let error: Api_t.error = { success = false; error = msg } in 
		error |> Api_j.string_of_error 

	let respond status body = 
		Server.respond_string ~status ~body () 		
	

	let create_account body = 			
		let timeout = tx_timeout () in 		

		let parse body : T.account_create_req = 
			let r = body |> Api_j.account_create_req_of_string in 
			{name=r.name; email=r.email}
		in					
		
		body |> parse |> Network.create_account ~timeout
		>>= function
			| Error e -> begin 
				(* map each error case to an error response *)
				match e with 
				| `CantRoute ->
					respond `I_m_a_teapot "AccountCreate service is down"
				| `Timeout ->
					respond `I_m_a_teapot "Gateway timeout"
				| `DecodeError ->				
					respond `I_m_a_teapot "Network fuckup..."					
			end
			| Ok result -> begin 
				match result with 
				| CreateAccountResp {id} ->
					respond `OK @@ Printf.sprintf "Account %d created!" id
				| Failed reason ->
					Log.info (fun f -> f "but why????");
					respond `I_m_a_teapot reason
				| _ -> 
					respond `OK "I was not looking for these results!"
			end


	let delete_account body = 
		let to_task body : T.task = 
			let r = body |> Api_j.account_delete_req_of_string in 
			DeleteAccountReq {id=r.account_id}
		in
		Log.info (fun f -> f "account delete!");	
		let timeout = tx_timeout () in 
		
		body |> to_task |> 
		(* send a task and wait till it returns something *)
		Network.rpc "account.delete" ~timeout
		>>= function 
		| Error e -> 
			(* we dont care about the cause *)
			respond `I_m_a_teapot "Sorry, we can't make it happen."
		| Ok result -> match result with 
			| Success -> 
				respond `OK "E!"
			| Failed reason -> 
				respond `OK (Printf.sprintf "ehhh? %s" reason)
			| Ack ->
				respond `OK "ack"
			| _ ->
				respond `OK "WTF?"


	let on_exceptions ex = 
		match ex with 
		| Rhyno_platform.Dispatch.Timeout ->
			Log.warn (fun f -> f "Request has timed out");
			respond `Gateway_timeout "Timeout"

		| Atdgen_runtime__Oj_run.Error msg ->
			Log.err (fun f -> f "Invalid input: %s" msg);
			respond `I_m_a_teapot ( error_message msg )
	
		| Yojson.Json_error msg -> 
			Log.err (fun f -> f "JSON decoding error: %s" msg);
	    	respond `I_m_a_teapot ( error_message msg )					

		| e ->
			let exn = Printexc.to_string e in 			
			Log.err (fun f -> f "Server error: %s" exn);				
			respond `Internal_server_error ( error_message exn )


	let serve http =
		let dispatch request body = 
			let uri = Cohttp.Request.uri request in		    					
			let uri = Uri.to_string uri in
			Cohttp_lwt.Body.to_string body >>= fun body ->
 				if uri = "//localhost:8000/create" then
					create_account body
				else if uri = "//localhost:8000/delete" then 
					delete_account body
				else 
					respond `OK ""
		in			
		let callback (_, cid) request body =
			Lwt.catch
				(fun () -> dispatch request body)
				on_exceptions
		in
		let conn_closed (_,cid) =
	  		let cid = Cohttp.Connection.to_string cid in
	  		Log.info (fun f -> f "HTTP [%s] closing" cid)
	  	in
		Log.info (fun f -> f "Starting HTTP server at port %d" (http_port ()));
		http (`TCP (http_port ())) @@ Server.make ~conn_closed ~callback ()

	let init conduit = 
		serve conduit

end
