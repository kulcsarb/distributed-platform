trap ctrl_c INT

DEBUG_LEVEL=debug

hub/hub --listen 10005 --links 127.0.0.1:10004 --logs=$DEBUG_LEVEL &