open Lwt.Infix
open Example_app

let log_src = Logs.Src.create ("ACCOUNT.CREATE") ~doc:"API"
module Log = (val Logs.src_log log_src: Logs.LOG)

module Make (Network: Wrapper.S) = struct
	
	let response id : T.result = 
		CreateAccountResp {id}

	let account_created_event id name : T.event = 
		AccountCreated {id; name; balance=0.0}

	let create_account account_id = 
		(* Implement it in a real application *)
		Lwt.return_unit 

	let on_task_account_create (header: T.header) (task: T.task) =		
		match task with
		| CreateAccountReq req ->		
			Log.info (fun f -> f "--> CreateAccountReq %s %s " req.name req.email);
			let account_id = 123 in 
			create_account account_id >>= fun () ->
			
			account_created_event account_id req.name
			|> Network.event "account.created"  >>= fun _ -> 

			response account_id 					
			|> Network.result_for header >>= function 
 			| Ok _ -> begin
				(* Network.remove_task_handler "account.create"; *)
				Log.info (fun f -> f "result was sent back");
				Lwt.return_unit
				end
 			| Error e -> begin
 				Log.warn (fun f -> f "Cannot send results !! ");
 				Lwt.return_unit
 				end

		| _ ->			
			Lwt.return_unit

end
