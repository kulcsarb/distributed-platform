module Main (CohttpServer: Cohttp_lwt.S.Server) (Ipv4:  Mirage_stack_lwt.V4) (Time: Mirage_time_lwt.S) = struct
			
	module Rhyno = Rhyno_platform.Api.Make (Ipv4) (Time)
	module Network = Example_app.Wrapper.Make (Rhyno)
	module Srv = Service.Make (Network)

	let start conduit ip_stack time =			

		Network.on_task "account.create" Srv.on_task_account_create;

		Rhyno.init ip_stack (Key_gen.listeners ()) (Key_gen.links ()) (Key_gen.status ());

end