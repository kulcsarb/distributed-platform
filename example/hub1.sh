trap ctrl_c INT

DEBUG_LEVEL=debug

hub/hub --listen 10001 --links 127.0.0.1:10000 --logs=$DEBUG_LEVEL &