trap ctrl_c INT

DEBUG_LEVEL=debug

function ctrl_c() {
   echo "** Trapped CTRL-C"
   pkill -9 api
   pkill -9 account_delete
   pkill -9 account_create
   pkill -9 account_events
   pkill -9 hub
}

api/api --listen 10000 --listen-http 8000 --logs=$DEBUG_LEVEL &

hub/hub --listen 10001 --links 127.0.0.1:10000 --logs=$DEBUG_LEVEL &
hub/hub --listen 10002 --links 127.0.0.1:10000 --logs=$DEBUG_LEVEL &
hub/hub --listen 10003 --links 127.0.0.1:10000 --logs=$DEBUG_LEVEL &
hub/hub --listen 10004 --links 127.0.0.1:10003 --logs=$DEBUG_LEVEL &
hub/hub --listen 10005 --links 127.0.0.1:10004 --logs=$DEBUG_LEVEL &

account_create/account_create --links 127.0.0.1:10001 --logs=$DEBUG_LEVEL &
account_create/account_create --links 127.0.0.1:10001 --logs=$DEBUG_LEVEL &
account_create/account_create --links 127.0.0.1:10002 --logs=$DEBUG_LEVEL &
account_create/account_create --links 127.0.0.1:10002 --logs=$DEBUG_LEVEL &

account_delete/account_delete --links 127.0.0.1:10003 --logs=$DEBUG_LEVEL &
account_delete/account_delete --links 127.0.0.1:10003 --logs=$DEBUG_LEVEL &
account_delete/account_delete --links 127.0.0.1:10003 --logs=$DEBUG_LEVEL &
account_delete/account_delete --links 127.0.0.1:10003 --logs=$DEBUG_LEVEL &


mailer/mailer --links 127.0.0.1:10005 --logs=$DEBUG_LEVEL &
mailer/mailer --links 127.0.0.1:10005 --logs=$DEBUG_LEVEL &
mailer/mailer --links 127.0.0.1:10004 --logs=$DEBUG_LEVEL &
mailer/mailer --links 127.0.0.1:10004 --logs=$DEBUG_LEVEL &



