OPAM = opam
DEPEXT ?= opam depext --yes --update rhyno_platform

.PHONY: all depend depends clean build
all:: build

depend depends::
	cd platform
	$(OPAM) pin add -k path --no-action --yes rhyno_platform .
	$(DEPEXT)
	$(OPAM) install --yes --deps-only rhyno_platform
	$(OPAM) pin remove --no-action rhyno_platform

build::
	cd platform
	dune build @install
	
install::
	cd platform
	dune install


clean::
	cd platform
	dune clean

