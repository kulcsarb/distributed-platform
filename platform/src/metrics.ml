(* TODO: make this module parametrizable to customize subsystem !!!! *)

include Prometheus

let namespace = "paygate"

let processed_messages = 
	let help = "# of processed messages" in
	Counter.v ~help ~namespace "processed_Types_total"

let error_wrong_message_type = 
	let help = "# of messages with wrong type" in
	Counter.v ~help ~namespace "error_wrong_message_type_total"

let error_wrong_message_format = 
	let help = "# of messages unable to decode" in
	Counter.v ~help ~namespace "error_wrong_message_format_total"

let msg_buffer_size = 
	let help = "# of messages waiting in the TX queue" in
	Gauge.v ~help ~namespace "tx_buffer_size"


let inc c = 
	Prometheus.Counter.inc_one c

let gauge_inc g =
	Prometheus.Gauge.inc_one g

let gauge_dec g =
	Prometheus.Gauge.dec_one g
	

module Make (Server: Cohttp_lwt.S.Server) = struct 

	module PrometheusServer = Prometheus_app.Cohttp(Server)
	
	let init http port =
	    let callback = PrometheusServer.callback in
	    match port with
			| None -> 
				Lwt.return_unit
			| Some port -> 				
	    		Logs.info (fun f -> f "Starting Prometheus server at %d" port);
	    		http (`TCP port) (Server.make ~callback ())

end

(* 
let add_to_threads port threads = 
	match port with
	| p when p = 0 -> threads
	| _ -> (start_server port) :: threads *)