[@@@ocaml.warning "-33"]
open T

let (>>=) a b = Lwt.bind a b

let log_src = Logs.Src.create "Rhyno.Relay" ~doc:"Rhyno Network"
module Log = (val Logs.src_log log_src: Logs.LOG)

module type Buffer = sig 
	val add: T.application_message -> unit 
end

module type D = sig
	val has_handler: string -> bool	
	val for_me: string -> bool
	val handle: T.application_message -> unit
end

module Make (DISPATCHER: D) = struct 

	let count = ref 0

	let choose ?(_min=1) connections = 		
		match connections with 
		| [] ->  []
		| conns -> 			
			let i = Random.int @@ List.length conns in
			[List.nth conns i]
 	
 	let choose_connections ?(without=None) message = 
 		match message.header.route with  		
		| All routing_key ->
			Routes.find_all ~without routing_key
		| One routing_key ->
			Routes.choose_one ~without routing_key
		| Direct node_id ->
 			match Nodes.find_opt node_id with 
			| Some node -> [node.conn]
			| None -> []

	let route_message ?(without=None) (message: T.message) =
		try
			match message.payload with
				| Application app_message -> (
					let connections = app_message |> choose_connections ~without in
					match connections with 
					| [] -> 
						Logs.debug (fun f -> f "%s - nowhere to forward" (Message.to_string message));
						Lwt_result.fail `CantRoute
					| _ ->						
						(* FIXME: handle error conditions better! *)
						Lwt_list.iter_p (fun conn -> 
							Log.debug (fun f -> f "%s - routing via [%d]" (Message.to_string message) conn#id);
							conn#send message >>= fun _ -> Lwt.return_unit
						) connections
						>>= fun () -> 
						Lwt_result.return ()
					)
				| _-> 
					Lwt_result.fail `CantRoute
		with | exn ->
			let exn = Printexc.to_string exn in
			Log.err (fun f -> f "%s - send failed: %s " (Message.to_string message) exn);
			Lwt_result.fail `CantRoute

	let send ~(route: route) ~(correlation_id: uuid) ~(msg_type: string) ~(payload: blob) =
		Message.create ~route ~correlation_id ~msg_type ~payload |> route_message
		
	let relay incoming_connection message =		
		message 
		|> Message.touch 
		|> route_message ~without: (Some incoming_connection) 
		>>= function 
			| Error _ -> Lwt.return_unit  (*TODO: what should I do with relaying errors ? *)
			| Ok _ -> Lwt.return_unit

	let handle (message: T.application_message) = 		
		DISPATCHER.handle message

	let decide_what_to_do_with (app_message: T.application_message) = 
		match app_message.header.route with 
		| Direct node_id -> (
			match DISPATCHER.for_me node_id with 
			| true -> `Handle
			| false -> `Relay
			)
		| All routing_key -> (
			match DISPATCHER.has_handler routing_key with 
			| true -> `HandleAndRelay
			| false -> `Relay
			)
		| One routing_key ->
			match DISPATCHER.has_handler routing_key with
			| true -> `Handle 			
			| false -> `Relay 			
			
	let on_message conn (message: T.message) =
		match Duplicate_checker.check message.id with 
		| true -> 
			Log.debug (fun f -> f "%s already handled" (Message.to_string message));
			Lwt.return_unit
		| false -> 

		match message.seen_by |> Routes.contains_my_origin with 
		| true ->
			Log.debug (fun f -> f "%s already seen" (Message.to_string message));
			Lwt.return_unit
		| false ->	

		match message.payload with 
		| Application app_message -> (
			count := !count + 1;
			Lwt.async (fun () -> Status.send ("COUNT " ^ Global.my_origin_str ^ "-" ^ (string_of_int !count)));
			match decide_what_to_do_with app_message with 
			| `Handle ->
				handle app_message;
				Lwt.return_unit
			| `Relay ->				
				relay conn message 
			| `HandleAndRelay ->
				handle app_message;
				relay conn message
			)
		| _ -> 
			Lwt.return_unit

end