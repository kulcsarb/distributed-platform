let log_src = Logs.Src.create "Rhyno.Connections" ~doc:""
module Log = (val Logs.src_log log_src: Logs.LOG)

let connections: (int * T.connection) list ref = ref []

let add conn = 		 	
	Log.info (fun f -> f "[%d] Connected to %s" conn#id conn#addr_s);	
	connections := [(conn#id, conn)] @ !connections

let remove conn = 
	Log.info (fun f -> f "[%d] Disconnected from %s" conn#id conn#addr_s);
	connections := List.remove_assoc conn#id !connections

let without conn = 
	List.fold_left (fun acc (id, c) -> if conn#id == id then acc else [c] @ acc) [] !connections

let all () =
	List.map (fun (_, c) -> c) !connections
