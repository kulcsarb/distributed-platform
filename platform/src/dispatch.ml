open Lwt.Infix
open T

let log_src = Logs.Src.create "Rhyno.Dispatch" ~doc:"API"
module Log = (val Logs.src_log log_src: Logs.LOG)

exception Timeout 

type promise_t = (T.application_message Lwt_mvar.t * Uuid.t)

module Promise (Time: Mirage_time_lwt.S) = struct 		

	let promises = Hashtbl.create 100000

	let correlation_id () = 
		Uuid.v `V4

	let create () : promise_t =
		let p = Lwt_mvar.create_empty () in
		let corr_id = correlation_id ()  in 
		Log.debug (fun f -> f "ADD %s" (Uuid.to_string corr_id));
		Hashtbl.add promises corr_id p;
		(p, corr_id)

	let find (corr_id: Uuid.t) = 
		Hashtbl.find_opt promises corr_id

	let remove corr_id = 
		Log.debug (fun f -> f "REMOVE %s" (Uuid.to_string corr_id));
		Hashtbl.remove promises corr_id

	let put p (value: T.application_message) =		
		Lwt_mvar.put p value

	let get p =
		Lwt_result.ok @@ Lwt_mvar.take p

	let wait_till secs = 
		Time.sleep_ns (Duration.of_sec secs) >>= fun () ->
		Lwt_result.fail `Timeout

	let wait_for (promise:promise_t) timeout =
		let p, corr_id = promise in 
		Log.debug (fun f -> f "WAIT FOR %s" (Uuid.to_string corr_id));
		Lwt.choose [get p; wait_till timeout] >>= fun (response) ->
		remove corr_id;
		Lwt.return response
		
	let fulfill (corr_id: Uuid.t) (response: T.application_message) = 
		match corr_id |> find with
		| Some p -> 
			Log.info (fun f -> f "RESULT %s fulfilled" (Uuid.to_string corr_id));
			put p response
		| None -> 
			Log.warn (fun f -> f "Cannot fulfill %s: promise not found!" (Uuid.to_string corr_id));
			Lwt.return_unit

end 



module Make (Time: Mirage_time_lwt.S) = struct 
	
	module PROMISE = Promise (Time) 	

	module Buffer = struct 

		let buffer, writer = Lwt_stream.create ()
		let size = ref 0 

		let get_size () = !size 

		let add (msg: T.application_message) = 
			size := !size + 1;
			Metrics.(gauge_inc msg_buffer_size);
			writer (Some msg)

		let next () : T.application_message Lwt.t = 
			Lwt_stream.next buffer >>= fun msg ->
			size := !size - 1;	
			Metrics.(gauge_dec msg_buffer_size);
			Lwt.return msg	

	end


	let create_promise () = 
		PROMISE.create ()

	let correlation_id () = 
		PROMISE.correlation_id ()
		
	(*  ===============================================

			external interface 

		=============================================== *)

	let has_routing_key routing_key = 
		List.mem_assoc routing_key !Global.callbacks

	let has_handler routing_key = 
		List.exists (fun (_, (_, re, _)) -> Routes.match_route routing_key re ) !Global.callbacks
	
	let for_me node_id = 
		Uuid.equal Global.my_origin node_id		


	let add_handler ~group ~routing_key f = 		
		let re = Routes.compile_re routing_key in 
		let already_added = has_routing_key routing_key in 
		Global.callbacks := [(routing_key, (group, re, f))] @ !Global.callbacks;
		if not already_added then 
			Router.on_routing_key_added routing_key group

	let remove_handler ~group ~routing_key =		
		Global.callbacks := List.remove_assoc routing_key !Global.callbacks;
		Router.on_routing_key_removed routing_key group

	let wait_for ?(timeout=1) (promise: promise_t) = 
		PROMISE.wait_for promise timeout 


	(* 	===============================================

			Handlers for incoming messages from network

		===============================================   *)

	let handle (msg: T.application_message) =
		Buffer.add msg
		
	let on_new_message (msg: T.application_message) = 
		let call_matching_handlers routing_key = 
			let call (_, (_, re, f)) =
				match Routes.match_route routing_key re with
				| true -> f msg
				| false -> Lwt.return_unit						
			in
			Lwt_list.iter_p call !Global.callbacks
		in
		match msg.header.route with 
		| Direct _ -> 
			PROMISE.fulfill msg.header.correlation_id msg
		| One routing_key ->
			call_matching_handlers routing_key
		| All routing_key ->
			call_matching_handlers routing_key


	let rec process_buffer () : unit Lwt.t = 
		Buffer.next () >>= fun msg -> 
		msg |> on_new_message >>= fun _ ->
		process_buffer ()

	let start () = 
		Log.info (fun f -> f "Dispatcher started");
		process_buffer ()

end