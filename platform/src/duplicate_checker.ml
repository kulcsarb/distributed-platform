let ids: (Uuid.t list) ref = ref [] 

let check (message_id: Uuid.t) : bool = 
	match (List.exists (fun id -> Uuid.equal id message_id) !ids) with 
	| true -> true
	| false ->			
		ids := !ids @ [message_id];
			if List.length !ids > 1000 then (
			ids := match !ids with 
			| [] -> []
			| _ :: tail -> tail
		);
		false