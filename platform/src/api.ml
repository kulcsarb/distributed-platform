open Lwt.Infix
open T

let task_prefix = "task."
let event_prefix = "event."

let log_src = Logs.Src.create "Rhyno.Api" ~doc:"Rhyno Network"
module Log = (val Logs.src_log log_src: Logs.LOG)


module type S = sig 
	val publish: ?correlation_id: string -> ?msg_type: string -> route -> blob -> (unit, [> `CantRoute]) Lwt_result.t
	val subscribe: ?group: string option -> string -> message_handler -> unit 
	val unsubscribe: ?group: string option -> string -> unit

	val on_task: ?group: string option -> string -> message_handler -> unit 
	val on_event: ?group: string option -> string -> message_handler -> unit 	

	val task: ?correlation_id: uuid option -> string -> blob -> (unit, [> `CantRoute]) Lwt_result.t
	val rpc: ?timeout:int -> string -> blob -> (application_message, [> `CantRoute | `Timeout]) Lwt_result.t
	val event: string -> blob -> (unit, [> `CantRoute]) Lwt_result.t
	
	val result_for: application_message_header -> blob -> (unit, [> `CantRoute]) Lwt_result.t
	
	val remove_task_handler: ?group: string option -> string -> unit
	val remove_event_handler: ?group: string option -> string -> unit 
	(* val init: Mirage_stack_lwt.V4.IPV4.t -> string -> int -> string list -> unit Lwt.t *)
end

module Make (STACK:  Mirage_stack_lwt.V4) (TIME: Mirage_time_lwt.S) = struct 
	
	module STATUS = Status.Make (STACK) (TIME)
	module DISPATCH = Dispatch.Make (TIME)	
	module RELAY = Relay.Make (DISPATCH)
	module NET = Network.Make (STACK) (TIME) (RELAY) 

(* ============================================================

					LOW LEVEL PUB/SUB INTERFACE
	
   ============================================================
 *)

	let publish ?(correlation_id="") ?(msg_type="") route payload =
		RELAY.send ~route ~correlation_id ~msg_type ~payload		

	let subscribe ?(group=None) routing_key f =
		let group = group or Global.my_origin_str in	
		DISPATCH.add_handler ~group ~routing_key f

	let unsubscribe ?(group=None) routing_key =
		let group = group or Global.my_origin_str in 		
		DISPATCH.remove_handler ~group ~routing_key


(* =============================================================

		HIGH LEVEL PUB/SUB INTERFACE FOR EVENTS, TASKS, AND RPC 

					RECEIVING MESSAGES

   	============================================================= *)


	let on_task ?(group=None) routing_key f = 
		let routing_key = task_prefix ^ routing_key in 
		let group = group or routing_key in 		
		DISPATCH.add_handler ~group ~routing_key f		
		
	let on_event ?(group=None) routing_key f = 
		let routing_key = event_prefix ^ routing_key in 
		let group = group or Global.my_origin_str in 		
		DISPATCH.add_handler ~group ~routing_key f		

	let remove_task_handler ?(group=None) routing_key = 
		let routing_key = task_prefix ^ routing_key in 
		let group = group or routing_key in 		
		DISPATCH.remove_handler ~group ~routing_key
		
	let remove_event_handler ?(group=None) routing_key = 
		let routing_key = event_prefix ^ routing_key in 
		let group = group or Global.my_origin_str in 		
		DISPATCH.remove_handler ~group ~routing_key
		

(* =============================================================

		HIGH LEVEL PUB/SUB INTERFACE FOR EVENTS, TASKS, AND RPC 

					PUBLISHING MESSAGES

   =============================================================  *)

	let task ?(correlation_id=None) routing_key payload =
		let correlation_id = correlation_id or DISPATCH.correlation_id () in 
		let routing_key = task_prefix ^ routing_key in 
		let route = One routing_key in		
		let msg_type = "task" in
		Log.info (fun f -> f "NEW TASK %s %s" (Uuid.to_string correlation_id) routing_key);
		RELAY.send ~route ~correlation_id ~msg_type ~payload
 
	let rpc ?(timeout=1) routing_key payload : (application_message, [> `CantRoute | `Timeout]) Lwt_result.t =
		let promise = DISPATCH.create_promise () in
		let (_, correlation_id) = promise in	
		let correlation_id = Some correlation_id in	
		task routing_key payload ~correlation_id >>= function
		| Ok _ ->
			DISPATCH.wait_for promise ~timeout
		| Error e -> 
			Lwt_result.fail e
			
	let event routing_key payload = 
		let routing_key = event_prefix ^ routing_key in
		let route = All routing_key in 		
		let correlation_id = DISPATCH.correlation_id () in		
		let msg_type = "event" in
		Log.info (fun f -> f "NEW EVENT %s" routing_key);
		RELAY.send ~route ~correlation_id ~msg_type ~payload

	let result_for (header: application_message_header) payload =
		let route = Direct header.origin in 
		let correlation_id = header.correlation_id in		
		let msg_type = "result" in
		Log.info (fun f -> f "NEW RESULT %s -> %s" (Uuid.to_string correlation_id) (Uuid.to_string header.origin));
		RELAY.send ~route ~correlation_id ~msg_type ~payload


	(* let _ = DISPATCH.start () *)

 	let init ip_stack listeners links status_address = 
 		Lwt.async (fun () -> STATUS.init ip_stack status_address);
 		TIME.sleep_ns (Duration.of_sec 1) >>= fun () ->
 		Lwt.join [
			DISPATCH.start ();
			NET.init ip_stack listeners links			
		]

end