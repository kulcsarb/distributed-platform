open Lwt.Infix 

let log_src = Logs.Src.create "Rhyno.Status" ~doc:""
module Log = (val Logs.src_log log_src: Logs.LOG)


let status_conn : T.status_connection option ref = ref None

let send (message: string) = 
	match !status_conn with 
	| None -> 
		Logs.warn (fun f -> f "cannot send status, connection is not ready");
		Lwt_result.fail ()
	| Some conn -> 
		conn#send (message ^ "\n")


module Make (STACK:  Mirage_stack_lwt.V4) (TIME: Mirage_time_lwt.S) = struct 

	class connection ?(reconnect=false) flow = 		
		let target_ip, target_port = STACK.TCPV4.dst flow in
		

		object(self: 'self)
			val addr = (target_ip, target_port)			
			val flow: STACK.TCPV4.flow = flow		
			val mutable closed: bool = false
			val mutable reconnect: bool = reconnect			
			val read_buffer = new Network_buffer.read_buffer (2)

			method serve = 		
				try
					(* Log.debug (fun f -> f "[%d] waiting for input...." id); *)
					STACK.TCPV4.read flow >>= function
			        | Ok `Eof -> 
			        	self#close "Connection closed by peer"
				    | Error _e -> 
				    	self#close "Error reading data from established connection"				        
				    | Ok (`Data _) ->				    	
				    	self#serve
				with
				| _ -> 					
					self#serve					

			method to_buffer (message: string) = 
				let page = Io_page.(to_cstruct (get 1)) in    			
    			Cstruct.blit_from_string message 0 page 0 (String.length message);
    			let buf = Cstruct.sub page 0 (String.length message) in
    			buf
			

			method send (message: string) = 
				if not closed then (	      							
					message 
					|> self#to_buffer
					|> STACK.TCPV4.write flow
					>>= function
	      			| Ok () -> 	      				
	      				Lwt_result.return ()
	      			| Error _ -> 
	      				self#close "Error while sending message" (* @@ string_of_addr addr *)      			
	      				>>= fun () ->
	      				Lwt_result.fail ()
				) else (
					Logs.warn (fun f -> f "connection closed, cant send message back");
					Lwt_result.fail ()
				)
				

			method close msg = 
				Logs.err (fun f -> f "%s" msg); 
				closed <- true;
		        STACK.TCPV4.close flow

		end


	let receive flow =		
		let connection = new connection flow in 	
		let conn = (connection :> T.status_connection) in 			
		
		status_conn := Some conn; 

		let routing_key = match !Global.callbacks with 
		| (routing_key, (_, _, _)) :: _ -> routing_key
		| [] -> ""
		in 
		
		connection#send ("ME " ^ Global.my_origin_str ^ "-" ^ routing_key ^ "\n") >>= fun _ ->
		connection#serve >>= fun () ->		

		status_conn := None;

		Lwt.return_unit


	let rec connect stack target_ip port =
		let t = STACK.tcpv4 stack in

 		STACK.TCPV4.create_connection t (target_ip, port) >>= function
    	| Error _err -> 
    		Logs.warn (fun f -> f "Status connection failed");
    		TIME.sleep_ns (Duration.of_sec 1) >>= fun () ->
    		connect stack target_ip port

    	| Ok flow ->
    		Logs.warn (fun f -> f "Status port connected");
    		receive flow >>= fun () ->
    		TIME.sleep_ns (Duration.of_sec 1) >>= fun () ->
    		connect stack target_ip port
	

	let init ip_stack address = 
		match String.split_on_char ':' address with
			| [ip; port] -> 
				connect ip_stack (Ipaddr.V4.of_string_exn ip) (int_of_string port)
			| _ -> 
				Lwt.return_unit

end
