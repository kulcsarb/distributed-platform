open Lwt.Infix 

let log_src = Logs.Src.create "Rhyno.Network" ~doc:""
module Log = (val Logs.src_log log_src: Logs.LOG)
	
module type R = sig 
	val on_message: T.connection -> T.message -> unit Lwt.t
end

module Make (STACK:  Mirage_stack_lwt.V4) (TIME: Mirage_time_lwt.S) (RELAY : R)= struct 


	let connection_counter = ref 0

	class connection ?(reconnect=false) flow = 		
		let target_ip, target_port = STACK.TCPV4.dst flow in
		let addr_s = Printf.sprintf "%s:%d" (Ipaddr.V4.to_string target_ip) target_port in

		object(self: 'self)
			val addr = (target_ip, target_port)
			val id = !connection_counter
			val flow: STACK.TCPV4.flow = flow		
			val mutable closed: bool = false
			val mutable reconnect: bool = reconnect			
			val read_buffer = new Network_buffer.read_buffer (2)

			method id = id
			method addr = addr
			method addr_s = addr_s			 			

			method on_routing_info (info: T.routing_info) =
				let address = match info.addr with 
					| None -> addr_s
					| Some a -> a
				in
				let info = {info with addr = Some address } in 
				let info = {info with hops = info.hops +1 } in 

				Router.on_routing_info (self :> T.connection) info >>= fun () ->

				Lwt.return_unit

				
			method parse =
				try
					match read_buffer#parse_message with 
					| None -> Lwt.return_unit
					| Some message ->
						(* Log.debug (fun f -> f "[%d] received <%s>" id (Uuid.to_string message.id)); *)
			    		(match message.payload with 
			    		| Routing info -> 
			    			self#on_routing_info info
			    		| Application _ -> 
			    			RELAY.on_message (self :> T.connection) message
			    		) >>= fun () ->
			    		self#parse
				with
				| _ -> 
					Log.warn (fun f -> f "[%d] unable to decode read buffer content (len: %d)" id read_buffer#content_len);
					Lwt.return_unit				
							    		

			method serve = 		
				try
					(* Log.debug (fun f -> f "[%d] waiting for input...." id); *)
					STACK.TCPV4.read flow >>= function
			        | Ok `Eof -> 
			        	self#close "Connection closed by peer"
				    | Error _e -> 
				    	self#close "Error reading data from established connection"
				        	(* Logs.warn (fun f -> f "Error reading data from established connection: %a" Stack.TCPV4.pp_error e); *)		        
				    | Ok (`Data buff) ->
				    	(* Log.debug (fun f -> f "[%d] read: %d bytes" id buff.len); *)
				    	read_buffer#add buff; 
				    	self#parse >>= fun () ->
				    	self#serve
				with
				| e -> 
					Log.warn (fun f -> f "[%d] serve: %s" id (Printexc.to_string e));
					self#serve					
			

			method send (message: T.message) = 
				if not closed then (	      		
					(* Log.debug (fun f -> f "[%d] sending <%s>" id (Uuid.to_string message.id));	 *)
					message 
					|> Network_buffer.write
					|> STACK.TCPV4.write flow
					>>= function
	      			| Ok () -> 	      				
	      				Lwt_result.return ()
	      			| Error _ -> 
	      				self#close "Error while sending message" (* @@ string_of_addr addr *)      			
	      				>>= fun () ->
	      				Lwt_result.fail ()
				) else (
					Logs.warn (fun f -> f "connection closed, cant send message back");
					Lwt_result.fail ()
				)
				

			method close msg = 
				Logs.err (fun f -> f "%s" msg); 
				closed <- true;
		        STACK.TCPV4.close flow

		end


	module Connection = struct 		

		let receive flow =
			connection_counter := !connection_counter + 1;
			let connection = new connection flow in 	
			let conn = (connection :> T.connection) in 			

			Connections.add conn;
			(* Status.send ("ADD " ^ Global.my_origin_str ^ "-" ^ connection#addr_s) >>= fun _ -> *)

			Router.on_connect conn >>= fun () ->
			
			connection#serve >>= fun () ->

			Router.on_disconnect conn >>= fun () ->
			
			Connections.remove conn;		
			(* Status.send ("REMOVE " ^ Global.my_origin_str ^ "-" ^ connection#addr_s) >>= fun _ -> *)

			Lwt.return_unit


		let rec make stack target_ip port =
			let t = STACK.tcpv4 stack in

	 		STACK.TCPV4.create_connection t (target_ip, port) >>= function
	    	| Error _err -> 
	    		(* Logs.warn (fun f -> f "Connection to port %s  failed" (string_of_addr (target_ip, port))) ; *)
	    		TIME.sleep_ns (Duration.of_sec 1) >>= fun () ->
	    		make stack target_ip port

	    	| Ok flow ->
	    		receive flow >>= fun () ->
	    		TIME.sleep_ns (Duration.of_sec 1) >>= fun () ->
	    		make stack target_ip port


	end
	
(* =============================================================

		PUBLIC INTERFACE	- 			INITIALIZERS

   =============================================================  *)

	let start_listeners ip_stack listeners =
		Lwt.join @@
			List.map (fun port -> 
				Log.info (fun f -> f "Starting listener on port %d" port);
				STACK.listen_tcpv4 ip_stack ~port (Connection.receive);
				STACK.listen ip_stack
			) listeners
		


	let establish_links ip_stack links = 
		Lwt_list.iter_p (fun address -> 			
				match String.split_on_char ':' address with
				| [ip; port] -> 
					Connection.make ip_stack (Ipaddr.V4.of_string_exn ip) (int_of_string port)
				| _ -> 
					Lwt.return_unit
			) links
	
	let status_connection ip_stack address = 
		match String.split_on_char ':' address with
			| [ip; port] -> 
				Connection.make ip_stack (Ipaddr.V4.of_string_exn ip) (int_of_string port)
			| _ -> 
				Lwt.return_unit


	let init ip_stack listeners links = 	
		Log.info (fun f -> f "Initializing");
		Random.self_init ();		
		Lwt.join [			
			start_listeners ip_stack listeners;
			establish_links ip_stack links
			]		

end