open Bigarray

let log_src = Logs.Src.create "Network.Buffer" ~doc:""
module Log = (val Logs.src_log log_src: Logs.LOG)

class read_buffer page_n =
	object(self: 'self)
		val buf = Io_page.(to_cstruct (get page_n))
		val mutable read_index = 0 
		val mutable write_index = 0		

		method content_len =
			write_index - read_index

		method add (input: Cstruct.t) = 
			if write_index + input.len > buf.len then 
				self#reset;

			let input_len = input.len in 
			let src = Array1.sub input.buffer input.off input.len in 
			let dest = Array1.sub buf.buffer write_index input_len in			
			(* Log.debug (fun f -> f "copy %d bytes at [%d]" input_len write_index); *)
			Array1.blit src dest;
			(* Log.debug (fun f -> f "write_index %d -> %d" write_index (write_index+input_len)); *)
			write_index <- write_index + input_len;
			


		method parse_message = 			
			if read_index = write_index then 
				None 
			else if read_index > write_index then (
				Log.err (fun f -> f "buffer overflow WTF is happening???");
				read_index <- write_index;
				None 
			) else (
				let pos_ref = ref read_index in 
				(* Log.debug (fun f -> f "parsing buffer at %d" !pos_ref); *)
				let msg = T.bin_read_message buf.buffer ~pos_ref in
				(* Log.debug (fun f -> f "read_index %d -> %d" read_index !pos_ref); *)
				read_index <- !pos_ref;
				Some msg
			)

		method reset = 
			if read_index = write_index then (
				read_index <- 0;
				write_index <- 0
				(* Do I really need this? *)
				(* ignore (Array1.fill buf.buffer 0) *)
			) else if read_index > write_index then (
				(* WTF???????? *)
				()
			) else (
				(* size of the fragment left in the buffer *)
				let frag_len = write_index - read_index in
				(* naming the fragment as src *)
				let src = Array1.sub buf.buffer read_index frag_len in 
				(* dst is the beginning of the buffer *)
				let dst = Array1.sub buf.buffer 0 frag_len in 	
				(* space to empty after the copy operation *)
				(* let _space = Array1.sub buf.buffer frag_len (buf.len - frag_len) in  *)
				(* copy fragment to the beginning of the buffer *)
				Array1.blit src dst;
				(* DO I NEED THIS??? *)
				(* Array1.fill space 0; *)
				(* resetting indexes *)
				read_index <- 0;
				write_index <- frag_len;
			)
	end

let write (msg: T.message) = 	
	let size = T.bin_size_message msg in
	(* creating new buffers every time... maybe maintaining a static buffer would be more efficient *)
	let write_buf = Cstruct.create_unsafe size 	in 
	ignore( T.bin_write_message write_buf.buffer ~pos:0 msg);				
	Cstruct.set_len write_buf size	