[@@@ocaml.warning "-33-26"]
open Bin_prot
open Bin_prot.Std

(* change it to Uuid.t and write serializers *)
type uuid = string
[@@deriving bin_io]


type routing_event_t = Add | Remove | Connect | Disconnect
[@@deriving bin_io]

type routing_info = { 
  event: routing_event_t;
  addr: string option;
  hops: int;
  routes: (string * string) list;
  origin : uuid;
  revision_no: int;
}
[@@deriving bin_io]


type blob = Payload.t
[@@deriving bin_io]

type route = 
  | One of string
  | All of string   
  | Direct of uuid
[@@deriving bin_io]

type application_message_header = {
  msg_type: string;
  route: route;
  origin: uuid;
  correlation_id: uuid;
}
[@@deriving bin_io]

type application_message = {
  header: application_message_header;  
  payload: blob;
}
[@@deriving bin_io]


type message_payload = 
  | Routing of routing_info 
  | Application of application_message
[@@deriving bin_io]

type message = {
  id: uuid;
  seen_by: uuid list;
  payload : message_payload;
}
[@@deriving bin_io]


type connection = < id: int;
          addr_s: string; 
          send: message -> (unit, unit) Lwt_result.t;
          >

type status_connection = <
          send: string -> (unit, unit) Lwt_result.t;
          >


type message_handler = (application_message -> unit Lwt.t)

type node_info = {
  origin: uuid;
  addr: string option;
  routes: (string * string) list ; 
  hops: int;
  revision_no: int;
  conn: connection;
  mutable load: int;
}

type route_index = {
  routing_key: string;
  group: string;
  re: Re.re;
}


let (or) a b = match a with | None -> b | Some x -> x
let some a = match a with | None -> false | Some _ -> true 