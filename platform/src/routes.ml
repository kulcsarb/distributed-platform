open T

let log_src = Logs.Src.create "Rhyno.Routes" ~doc:""
module Log = (val Logs.src_log log_src: Logs.LOG)


let index: ((uuid * route_index) list) ref = ref []

let dot_re = Re.Posix.compile_pat "\\."
let asterix_re = Re.Posix.compile_pat "\\*"

let filter = List.filter
let map = List.map

let compile_re routing_key = 
	"^" ^ routing_key ^ "$"
	|> Re.replace_string dot_re ~by: "\\."
	|> Re.replace_string asterix_re ~by: "[a-zA-Z0-9_-]+"
	|> Re.Posix.compile_pat

let for_me id =
	Global.my_origin = id

let contains_my_origin node_id_list = 
	List.exists for_me node_id_list

let include_route routing_key routing_keys =
	match routing_key with 
	| None -> false 
	| Some routing_key -> List.mem_assoc routing_key routing_keys	

let match_route routing_key re =
	List.length (Re.matches re routing_key) > 0

let match_routes routing_key re_list = 		
	List.exists (fun (_, re) -> match_route routing_key re) re_list

let idx_of group routing_key : route_index = 
	{routing_key; group; re=compile_re routing_key}

let remove_all_index origin = 
	index := List.filter (fun (o, _) -> o <> origin) !index

let remove_missing_index node =
	index := List.filter (fun (origin, {routing_key; group; _}) -> 
		match node.origin = origin with		
		| true -> 
			not (List.mem (group, routing_key) node.routes)
		| false -> true 
	) !index

let update_index node = 
	remove_missing_index node ;
	let current_routes = 
		!index
		|> List.filter (fun (origin, _) -> origin = node.origin)
		|> List.map (fun (_, {routing_key; group; _}) -> (group, routing_key)) 
	in
	let add (group, routing_key) = 
		if not (List.mem (group, routing_key) current_routes) then (
			index := [(node.origin, idx_of group routing_key)] @ !index
		)
	in 		
	List.iter add node.routes
	

let store (route: routing_info) conn =	
	let node: node_info = {	
			origin=route.origin;
			routes=route.routes;
			addr=route.addr;
			hops=route.hops;
			revision_no=route.revision_no;
			conn=conn;
			load=0
	} in 	
	Nodes.put node;
	update_index node;
	Nodes.debug "UPDATE"

let remove origin = 
	Nodes.remove origin;
	remove_all_index origin;
	Nodes.debug "REMOVE"


let add conn (route: routing_info) =	
	match route.origin |> for_me with 
	| true -> `ItsMe
	| false ->
		match Nodes.find_opt route.origin with 
		| None ->			
			store route conn;			
			if route.hops = 1 then (
				Lwt.async (fun () -> Status.send ("ADD " ^ Global.my_origin_str ^ "-" ^ (Uuid.readable route.origin)))
			);
			`NewRoute
		| Some node ->
			if route.revision_no > node.revision_no || route.hops < node.hops then (
				Log.debug (fun f -> f "%s routing info updated -> [%d]" (Uuid.readable route.origin) conn#id);
				store route conn;
				`NewRoute			
			) else 
				`AlreadySeen
	

let removeable ?(revision_no=max_int) origin conn_id = 
	match Nodes.find_opt origin with 
	| None -> false
	| Some node ->
		node.conn#id = conn_id && node.revision_no < revision_no


let remove_node conn origin = 
	match removeable origin conn#id with 
	| false -> 
		`NotFound
	| true ->
		remove origin;
		`NodeRemoved		

let remove conn (route: routing_info) = 					
	match removeable route.origin conn#id ~revision_no: route.revision_no with 
	| false -> 
		`NotFound
	| true ->
		match route.event with 
		| Disconnect -> 
			remove route.origin;
			`NodeRemoved
		| Remove ->
			store route conn;
			`RouteRemoved
		| _ ->
			`AlreadySeen


let find_routes ?(without=None) routing_key : (string * node_info) list= 
	let without_source = match without with 
	| None -> fun _ -> true
	| Some conn -> fun (_, n) -> not (n.conn#id = conn#id)
	in 
	let select_matching_indexes (_, ({re; _}: route_index)) : bool =
		match_route routing_key re
	in	

	List.filter select_matching_indexes !index
	|> List.map (fun (origin, {group; _}) -> (group, Nodes.find origin))
	|> List.filter without_source


let find_all ?(without=None) routing_key = 
	find_routes ~without routing_key
	|> List.map (fun (_, node) -> node.conn)


let choose_one ?(without=None) routing_key = 
	let matched_indexes = find_routes ~without routing_key in 
	let by_group_and_select_count (g1, n1) (g2, n2) = 
		match Pervasives.compare g1 g2 with
		| 0 -> Pervasives.compare n1.load n2.load
		| c -> c
	in
	let first_from_each_group acc (group, node) = 	
		match acc with 
		| [] -> 
			[(group, node)] @ acc
		| (prev, _) :: _ -> 
			if prev <> group then [(group, node)] @ acc else acc
	in 
	let sorted = List.sort by_group_and_select_count matched_indexes in 
	let selected = sorted |> List.fold_left first_from_each_group [] in 
	selected |> List.iter (fun (_, node) -> node.load <- node.load+1);
	selected |> List.map (fun (_, node) -> node.conn)
	
	
