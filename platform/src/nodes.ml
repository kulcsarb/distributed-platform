open T

let log_src = Logs.Src.create "Rhyno.Nodes" ~doc:""
module Log = (val Logs.src_log log_src: Logs.LOG)

let nodes: (uuid, node_info) Hashtbl.t = Hashtbl.create 1000

let put (node: node_info) = 
	Hashtbl.replace nodes node.origin node

let remove origin = 
	Hashtbl.remove nodes origin

let find_opt origin = 
	Hashtbl.find_opt nodes origin

let find origin =
	Hashtbl.find nodes origin

let connection_of origin = 
	match find_opt origin with 
	| None -> None 
	| Some node -> 
		(* Log.info (fun f -> f "%s found, hops: %d" (node_str node.origin node.conn) node.hops); *)
		Some node.conn



let str (node: node_info)  = 
	Printf.sprintf "%s -[%d]-> " (Uuid.readable node.origin) node.conn#id

let routes_str routes = 
	String.concat "," @@ List.map (fun (g, r) -> "|" ^ g ^ "|/" ^ r) routes


let debug title =
	let print_node node = 
		Log.debug (fun f -> f "%s %s" (node |> str) (routes_str node.routes))
	in				
		Log.debug (fun f -> f " ---- NODE %s ----" title);
		Hashtbl.iter (fun _ node -> print_node node) nodes;
		Log.debug (fun f -> f " ------------------")
