open Lwt.Infix 
open T

let log_src = Logs.Src.create "Rhyno.Router" ~doc:"Rhyno Network"
module Log = (val Logs.src_log log_src: Logs.LOG)

let revision_no = ref 0

let inc_revision () = 
	revision_no := !revision_no + 1  

let async f = Lwt.async f

let route ?(addr=None) ?(hops=0) ?(routes=[]) ?(origin=Global.my_origin) event = 
	{event; addr; hops; routes; origin; revision_no= !revision_no}

let route_of (node: T.node_info) event =
	route ~addr: node.addr ~hops: node.hops ~routes: node.routes ~origin: node.origin event

let (==>) route conn = route |> Message.wrap_route |> conn#send >>= fun _ -> Lwt.return_unit

let (==>>) route connections =
	Lwt_list.iter_p (fun conn -> route ==> conn) connections

let (|==>>) route connections  = 
	async (fun () -> route ==>> connections)


let my_routes () = 
	!Global.callbacks 
	|> List.map (fun (routing_key, (group, _, _)) -> (group, routing_key))

let propagate_my_routes conn = 	
	let routes = my_routes () in
	route Connect ~routes ==> conn

let propagate_routing_table conn = 
	Nodes.nodes
	|> Hashtbl.iter (fun _ (node: T.node_info) -> 
		if not (node.conn#id = conn#id) then async (
			fun () -> Connect |> route_of node ==> conn 
		))


let on_connect (conn: connection) = 		
	(*  
	1. send node_messages of all known nodes
	2. send my routes 
	3. send known routes
	*)
	propagate_my_routes conn >>= fun () ->

	propagate_routing_table conn;

	Lwt.return_unit


let on_disconnect (conn: connection) = 		
	let other_connections = Connections.without conn in 

	let remove_and_notify _ (node: T.node_info)  =
		match Routes.remove_node conn node.origin with 
		| `NodeRemoved ->
			Disconnect |> route_of node |==>> other_connections		
		| _ -> 
			()
	in 
	Hashtbl.iter remove_and_notify Nodes.nodes;

	Lwt.return_unit

let on_routing_key_added group routing_key =
	Log.info (fun f -> f "Routing key |%s|-%s added, broadcasting routing infos..." group routing_key);	
	inc_revision ();
	let routes = my_routes () in
	Add |> route ~routes |==>> (Connections.all ())


let on_routing_key_removed group routing_key =
	Log.info (fun f -> f "Routing key |%s|-%s removed, broadcasting routing infos..." group routing_key);	
	inc_revision ();
	let routes = my_routes () in
	Remove |> route ~routes |==>> (Connections.all ())


let on_routing_info (conn: T.connection) (route: T.routing_info) =	
	let other_connections = Connections.without conn in 
	match route.event with
	| Add | Connect -> begin 
		match Routes.add conn route with 
		| `NewRoute -> 
			route ==>> other_connections
		| _ ->
			Lwt.return_unit
		end
	| Remove | Disconnect -> begin
		match Routes.remove conn route with 
		| `RouteRemoved | `NodeRemoved ->
			route ==>> other_connections
		| _ ->
			Lwt.return_unit			
		end
