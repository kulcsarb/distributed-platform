open Bin_prot

type t = Bin_prot.Blob.Opaque.String.t

[@@deriving bin_io]

let size_of (p: t) = 
	Blob.Opaque.String.bin_size_t p

let create_buf n = 
	Bigarray.Array1.create Bigarray.char Bigarray.c_layout n

let to_buff (p: t) =   
	let buf = create_buf @@ size_of p in 
	ignore(Blob.Opaque.String.bin_write_t buf ~pos:0 p);
	buf

let decode (payload: t) bin_reader =
	try	
		let pos_ref = ref 8 in 
		payload
		|> to_buff
		|> bin_reader ~pos_ref		
		|> Some 
	with e ->
		Logs.err (fun f -> f "decode ex: %s" @@ Printexc.to_string e);
		None

let encode thing bin_size bin_write : t = 	
	let thing_size = thing |> bin_size in 
	let header_size = Utils.size_header_length in 
	let buf = create_buf (thing_size + header_size) in 	
	let pos = Utils.bin_write_size_header buf ~pos:0 thing_size in 
	ignore( bin_write buf ~pos thing);	
	let pos_ref = ref 0 in 
	Blob.Opaque.String.bin_read_t buf ~pos_ref	