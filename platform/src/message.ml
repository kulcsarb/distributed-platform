open T


let new_id () = 
	Uuid.v `V4

let to_string (message: message) = 
	let message_id = Uuid.to_string message.id in 
	match message.payload with 
	| Routing _ -> Printf.sprintf "<ROUTING %s>" message_id
	| Application m -> 
		let target = match m.header.route with 
			| Direct n -> (Uuid.to_string n)
			| All r -> r 
			| One r -> r
		in		
		Printf.sprintf "<%s %s -> %s" m.header.msg_type message_id target

let touch (message: message) = 
	{message with seen_by = [Global.my_origin] @ message.seen_by}	

let wrap (payload: message_payload) : message = 
	{
		id = new_id ();
		seen_by = [Global.my_origin];
		payload
	}	

let create ~(route: route) ~(correlation_id: uuid) ~(msg_type: string) ~(payload: blob) = 
	let header = { route; correlation_id; msg_type; origin=Global.my_origin} in 
	let message = { header; payload } in 
	wrap (Application message)

let wrap_route (r: routing_info) = 
	wrap (Routing r)

