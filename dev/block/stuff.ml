type ('a, 'b) my_t = [`String of 'a | `A of 'b];;

type my_t2 = [(int, string) my_t | `Int];;
type my_t3 = [(float, int) my_t | `Int];;
type my_t4 = [(int list, int list) my_t | `Int];;

let a = function #my_t -> "string" |#my_t2 -> "int";;

let () =
	
	print_endline @@ a (`String 1)
