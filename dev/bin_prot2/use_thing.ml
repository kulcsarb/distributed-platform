[@@@ocaml.warning "-33-26"]
open Bin_prot
open Bin_prot.Std

type account_created = {account_id: int; email: string}
[@@deriving bin_io]

type account_deleted = {account_id: int;}
[@@deriving bin_io]

type account_event = 
	| AccountCreated of account_created
	| AccountDeleted of account_deleted
[@@deriving bin_io]

type event_t = account_event Blob.t Thing.event
[@@deriving bin_io]

type event_whocares = Blob.Opaque.Bigstring.t Thing.event 
[@@deriving bin_io]

type event_string = Blob.Opaque.String.t Thing.event 
[@@deriving bin_io]


let cstruct_of_event_t e = 
	let size = bin_size_event_t e in
	let buf = Thing.create_buf size in
	ignore( bin_write_event_t buf ~pos:0 e);
	Cstruct.of_bigarray buf

let j_event: Event_t.event = {
	id = 99;
	routing_key = "lofasz";
	payload = `AccountCreated {account_id = 99; email="kulcsarb@gmail.com"}
}

let event_json = Event_j.string_of_event j_event

let () = 
	let e: event_t = {
			routing_key="account.created";
			id=23; 
			payload= AccountCreated {account_id=123; email="kulcsarb@gmail.com"}
			} 
	in

	let event_cstruct = cstruct_of_event_t e in
	Printf.printf "cstruct %s\n\n" @@ Cstruct.to_string event_cstruct;
	let pos_ref = ref 0 in 
	let event_wh = bin_read_event_whocares event_cstruct.buffer ~pos_ref in
	Printf.printf "event_wh:  %d %s, size: %d \n" event_wh.id event_wh.routing_key (bin_size_event_t e);
	let payload = event_wh.payload in 
	Printf.printf "payload size: %d, event_size: %d\n" (Blob.Opaque.Bigstring.bin_size_t event_wh.payload) (bin_size_account_event e.payload);
	
	let event_size = bin_size_account_event e.payload in
	let payload_size = (Blob.Opaque.Bigstring.bin_size_t event_wh.payload) in
	let diff = payload_size - event_size in
	let pos_ref = ref 0 in 	

	let mb = 1024. *. 1024. in
	let n = 100_000 in

	let t1 = Unix.gettimeofday () in
  	let buff = Thing.create_buf 100 in 
	for _ = 1 to n do
		pos_ref := 0;
		let e = bin_read_event_t event_cstruct.buffer ~pos_ref in 
		match e.payload with 
		| AccountCreated e -> assert (e.account_id = 123)
		| AccountDeleted _ -> ()
  	done;
  	let t2 = Unix.gettimeofday () in
  	let time_event_t = t2 -. t1 in


	let t1 = Unix.gettimeofday () in  	
	for _ = 1 to n do
		ignore( Event_j.event_of_string event_json  )	
  	done;
  	let t2 = Unix.gettimeofday () in
  	let time_json = t2 -. t1 in


  	let t1 = Unix.gettimeofday () in
  	let buff = Thing.create_buf 100 in 
	for _ = 1 to n do		
		pos_ref:=0;
		let event_wh = bin_read_event_whocares event_cstruct.buffer ~pos_ref in
		ignore( Blob.Opaque.Bigstring.bin_write_t buff ~pos:0 payload);
    	pos_ref := diff;
    	let payload = bin_read_account_event buff ~pos_ref in 
    	match payload with 
		| AccountCreated e -> assert (e.account_id = 123)
		| AccountDeleted _ -> ()
  	done;
  	let t2 = Unix.gettimeofday () in
  	let time_bigs = t2 -. t1 in


  	let t1 = Unix.gettimeofday () in
  	let buff = Thing.create_buf 100 in 
	for _ = 1 to n do
		pos_ref := 0;
		let e = bin_read_event_string event_cstruct.buffer ~pos_ref in
		ignore(Blob.Opaque.String.bin_write_t buff ~pos:0 e.payload);
		pos_ref := diff;
		let payload  = bin_read_account_event buff ~pos_ref in 
		match payload with 
		| AccountCreated e -> assert (e.account_id = 123)
		| AccountDeleted _ -> ()
  	done;
  	let t2 = Unix.gettimeofday () in
  	let time_str = t2 -. t1 in


  	pos_ref := 0;
	let e = bin_read_event_string event_cstruct.buffer ~pos_ref in 
	Printf.printf "OPAQUE STR %d %s\n%!" e.id e.routing_key;

  	let f_n = float n in
  	let msg_size = float (n * payload_size) in
	  	Printf.printf
	    "msgs: %d  msg length: %d\n\
	    Event decode: \n\
	    time: %.3fs  rate: %9.2f msgs/s  throughput: %.2f MB/s\n\
	    JSON decode: \n\
	    time: %.3fs  rate: %9.2f msgs/s  throughput: %.2f MB/s\n\
	    OPAQUE BS + payload decode: \n\
	    time: %.3fs  rate: %9.2f msgs/s  throughput: %.2f MB/s\n\
	    OPAQUE STR  + payload decode: \n\
	    time: %.3fs  rate: %9.2f msgs/s  throughput: %.2f MB/s\n\
	    %!"
	    n payload_size
	    time_event_t (f_n /. time_event_t) (msg_size /. time_event_t /. mb)
	    time_json (f_n /. time_json) (msg_size /. time_json /. mb)
	    time_bigs (f_n /. time_bigs) (msg_size /. time_bigs /. mb)
	    time_str (f_n /. time_str) (msg_size /. time_str /. mb)
	    
