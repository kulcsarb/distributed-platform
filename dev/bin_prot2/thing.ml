[@@@ocaml.warning "-33-26"] 
open Bin_prot
open Bin_prot.Std


type 'a event = {
	id: int;
	routing_key: string; 
	payload : 'a;	
} [@@deriving bin_io]


let create_buf n = Bigarray.Array1.create Bigarray.char Bigarray.c_layout n

 
