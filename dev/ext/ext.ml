[@@@ocaml.warning "-26-27-34"]

(* T.t is extendable, so I can add new variants to it *)
type T.t += Something
type T.t += Valami of int

module V = T.Msg.Constr (struct type t = int end)
module W = T.Msg.Constr (struct type t = float end)

let () = 
	let w = V.C 3 in
	let w = W.C 1.2323 in 
	begin match w with 
 	| W.C _ -> print_endline "injected with functor W.C"
	| V.C _ -> print_endline "injected with functor: V.C: "		
	| _ -> print_endline "???"
	end;
	(* let v = T.A.make in  *)
	let[@ocaml.warning "-26"] v = Something in 
	(* Exc is defined in module T  *)
	(* let v = T.Exc 1 in  *)
	(* variant Ize is declared as private, so I must call its constructor through a function *)
	let v = T.B.make "dasd" in
	match v with 
	| Something -> print_endline "Something"
	| Valami _ -> print_endline "Valami"
	| T.Exc _ -> print_endline "Exc"
	| T.B.Ize _ -> print_endline "private Ize"
 	| _ -> print_endline "???"

	