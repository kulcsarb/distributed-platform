module Msg : sig 
		type t = private ..
		module Constr(X: sig type t end) : sig 
			type t += C of X.t
			val make: X.t -> t
		end
	end = struct
		type t = ..
		module Constr(X: sig type t end) = struct 
			type t += C of X.t

			let make (v: X.t) : t= 
				C v
		end	
end

type t = .. 
type t += Exc of int

module type BS = sig 
	type t += private Ize of string
	val make : string -> t
end

module B : BS = struct 
	type t += Ize of string
	let make s = Ize s
end


(*
	private has to be declared in the signature, not in the implementation!
	declaring a variant private in the implementation will prevent in to be instanciated 
	even in the same module!!
module A = struct
	type t += private Mytype
	let make = 
		Mytype
end

*)