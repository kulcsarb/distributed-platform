type _ g = 
	| Float : float -> float g
	| Int : int -> int g

let x = Float 1.34
let y = Int 1

let get_float (v: float g) : float = 
	match v with 
	| Float f -> f

let get (type e) (t:e g) : e = 
	match t with 
	| Float f -> f
	| Int i -> i

let () = 
	Printf.printf "Fget:   %f\n" (get_float (Float 123.545));
	Printf.printf "X:  %f\n" (get x);
	Printf.printf "Y:  %d\n" (get y)



type 'a value = 
	| Float : float -> float value
	| Int : int -> int value
	| String : string -> string value
	| Array : 'a list -> 'a value

let get (type x) (v: x value) : x = 
	match v with 
	| Float f -> f
	| Int i -> i 
	| String s -> s
	| Array a -> List.nth a 0


let () = 
	let a = Array [2;4;5;5] in 
	let a2 = Array ['c';'d';'e'] in

	Printf.printf "A:   %d\n" (get a);
	Printf.printf "A2:   %c\n" (get a2);
	Printf.printf "F:   %f\n" (get (Float 234.5345));
	Printf.printf "I:   %d\n" (get (Int 45645));
	Printf.printf "S:   %s\n" (get (String "sfsdf"))
