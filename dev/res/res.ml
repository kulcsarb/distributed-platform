let (>>=) = Lwt.bind
let (>|=) = Lwt.map 

let a () = 
	if true then Lwt_result.return 1 else Lwt_result.fail `Lofasz


let b ?(f=fun () -> ()) () = 
	f ()

let main () = 

	a () >>= function
	| Ok _ -> Lwt.return @@ print_endline "OK "
	| Error e -> match e with 
		| `Lofasz -> Lwt.return @@ print_endline "Lofasz happened!!"
		| _ -> Lwt.return @@print_endline "something else happened"
	>>= fun _ ->
	Lwt.return_unit


let () = 
	Lwt_main.run @@ main ()